### 互联网核心

* 删除文件中包含指定内容的行
```shell
sed -i "/INTO\ \`laagent/d" file  ##删除包含 INTO `laagent` 的行
```

* 查看 SQL 脚本中包含的表及数据行数
```shell
for i in `grep 'CREATE TABLE ' test.sql | awk {'print $3'}`;do echo $i;  grep $i test.sql | wc -l;done
```

* 电商平台生成的密钥只能从明天开始，当前请求无法使用
* 修改 Mariadb 连接数限制
```
vim /usr/lib/systemd/system/mysqld.service
LimitNOFILE=65535
LimitNPROC=65535
```
